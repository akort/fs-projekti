/* eslint-disable no-undef */
/* eslint-disable no-mixed-spaces-and-tabs */
const webpack = require("webpack")

module.exports = {
	entry: "./src/index.js",
	module: {
		rules: [
		  {
				test: /\.(js|jsx)$/,
				exclude: [/node_modules/, /test/],
				use: ["babel-loader"]
		  }
		]
	  },
	  resolve: {
		extensions: ["*", ".js", ".jsx"]
	  },
	output: {
	  path: __dirname,
	  publicPath: "/",
	  filename: "bundle.js"
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin()
	],
	devServer: {
	  contentBase: "./dist",
	  hot: true
	}
};