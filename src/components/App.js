import React from "react"
import Map from "./Map"
import Vehicles from "./Vehicles"

class App extends React.Component {
	constructor() {
		super()

		this.state = {
			showMap: true
		}
	}

	toggleMap = (e) => {
		e.preventDefault()

		this.setState(previous => ({
			showMap: !previous.showMap}))
	}

	render() {
		const buttonStyle = {
			position:"fixed",
			left:0,top:0,zIndex:1000
		}
		return (
			<div>
				{this.state.showMap ?
					<Map />
					: <Vehicles/>}
				<button style={buttonStyle} onClick={e => this.toggleMap(e)} > Change page</button>
			</div>
		)
	}
}

export default App