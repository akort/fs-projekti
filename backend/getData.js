// eslint-disable-next-line no-undef
const axios = require("axios")

const baseUrl = "https://api.digitransit.fi/routing/v1/routers/hsl/index/graphql"

const getAll = async () => {
	let dataDisplayed = {
		query: `
		{
			bikeRentalStations {
			  name
			  bikesAvailable
			  stationId
			}
		}
	`
	}
	const request = await axios.post(baseUrl,dataDisplayed)
	return request.data
}

// eslint-disable-next-line no-undef
module.exports = {getAll}